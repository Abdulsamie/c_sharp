﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {   //Alles, was mit ReadLine eingegeben wird, wird als string gelesen!
            string x;
            Console.Write("Text1:");
            x = Console.ReadLine();
            Console.WriteLine(x);

            int y;
            Console.Write("Zahl1:");
            y = Convert.ToInt32(Console.ReadLine()); //1.Möglichkeit zur Umwandlung

            Console.WriteLine(y + 5);
            Console.Write("Zahl2:");
            
            y = int.Parse(Console.ReadLine());  //2.Möglichkeit zur Umwandlung
            Console.WriteLine(y + 5);
            Console.WriteLine("Zahl2 + 5 = {0}\n", y + 5);
            
            Console.WriteLine("or-operation");
            Console.WriteLine(0 | 1);
            
            Console.WriteLine("and-operation");
            Console.WriteLine(0&1);

            // 0 oder 1 werden nicht als boolean betrachtet!
            Console.Write("Not-operation, gebe true oder false:");
            bool t=bool.Parse(Console.ReadLine());
            Console.WriteLine("{0}! = {1}\n" ,t, !t);
            
            // Wird nicht funktionieren, weil f kein bool ist! und not oporator nur mit bool arbeitet!
/*            Console.Write("Not-operation, gebe 1 oder 0:");
            int f = int.Parse(Console.ReadLine());
            Console.WriteLine("{0}! = {1} ", f, !f);*/
        }
    }
}
