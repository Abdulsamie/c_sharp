﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {   //Wichtig: zur Rückgabe von mehreren Variablen in einer Methode, kann in, ref oder out verwendet werden\
            //Alle drei erstellen einen Verweis auf die Adresse der Variable im RAM

            /*  out: Diese Methode legt den Wert des Arguments fest, das als dieser Parameter verwendet wird, muss im Aufruf auch eingegeben werden
                ref: Diese Methode kann den Wert des Arguments ändern, das als dieser Parameter verwendet wird.
                in: Diese Methode ändert den Wert des Arguments nicht, das als dieser Parameter verwendet wird, muss nicht im Aufruf eingegeben werden.*/

            //A(1): Note eines Modul überprüfen, ist größer oder gleich 60, pass ausgeben, sonst fail
            int x;
            Console.Write("Bestehen eines Moduls überprüfen:\nNote= ");
            x = int.Parse(Console.ReadLine());
            point_check(x);
            
            //A(2): Schreiben Sie ein Programm zur Berechnung der Fläche und des Umfangs eines Kreies
            float r, pi=3.14f, Umfang, Fläche;
            Console.Write("Berechnung von Umfang und Fläche eines Kreises:\nRadius= ");
            r = float.Parse(Console.ReadLine());

            //Rückgabe von mehreren Variablen erfolgt mit Eingabe von out Schlüsselwort in dem Aufruf \
            //und der Methodendefinition, wobei der Rückgabewert void sein muss!
            Kreis(r, pi, out Umfang, out Fläche);
            Console.WriteLine("Umfang = {0}\nFläche =  {1}m²", Umfang, Fläche);

            //A(3):  Schreiben Sie ein Programm zur Berechnung der Fläche und des Umfangs eines Rechtecks
            float b, l, U, F;
            Console.Write("Berechnung von Umfang und Fläche eines Rechtecks:\nBreite= ");
            b = float.Parse(Console.ReadLine());

            Console.Write("Länge= ");
            l = float.Parse(Console.ReadLine());

            // zurückgegebene Fläche in F speichern
            F = Rechteck(b, l, out U);
            Console.WriteLine("Umfang = {0}\nFläche =  {1}m²", U, F);

            //A(4): Schreiben Sie ein Programm zum Abfragen von Vor- und Nachname und sie ausgeben
            string Vorname, Nachname;
            Console.Write("Eingabe von Vor- und Nachname\nVorname: ");
            Vorname = Console.ReadLine();

            Console.Write("Nachname: ");
            Nachname = Console.ReadLine();
            print_Name(Vorname, Nachname);

            //A(5): Berechnung von Mittelwert von drei Zahlen
            int x1, x2, x3;
            Console.Write("Berechnung von Mittelwert von drei Zahlen\nx1= ");
            x1 = int.Parse(Console.ReadLine());
            Console.Write("x2= ");
            x2 = int.Parse(Console.ReadLine());
            Console.Write("x3= ");
            x3 = int.Parse(Console.ReadLine());
            print_Mittelwert(x1, x2, x3);

            //A(5): Berechnung von Volumen eine Zylinder
            float r1, h, Volumen;
            Console.Write("Berechnung von Volumen eines Zylinders:\nRadius Grundfläche= ");
            r1 = float.Parse(Console.ReadLine());
            Console.Write("Höhe= ");
            h = float.Parse(Console.ReadLine());
            Volumen = Zylinder_Volumen(r1, pi, h);
            Console.WriteLine("Volumen = {0}m³", Volumen);
        }

        static void point_check(int t)
        {
            if (t <= 100 & t >= 0)
            {
                if (t >= 60)
                    Console.WriteLine("pass\n");
                else
                    Console.WriteLine("fail\n");
            }

            else
                Console.WriteLine("ungültige Note!\n");

        }

        // Nur mit out zurückgeben
        static void Kreis(float r, float pi, out float u, out float f)
        {
            u = 2 * pi * r;
            f = pi * r * r;
        }

        // mit out und return gleichzeitig zurückgeben
        static float Rechteck(float b, float l, out float u)
        {
            float f;
            u = 2 * b + 2 * l;
            return f = b*l;
        }

        static void print_Name(string V, string N)
        {
            Console.WriteLine('\n'+V + ' ' + N);
            Console.WriteLine("\n{0} {1}",V, N);

        }

        static void print_Mittelwert(int x1,int x2,int x3)
        {
            Console.WriteLine("Mittelwert = {0}", (x1 + x2 + x3) / 3);
        }

        static float Zylinder_Volumen(float r1, float pi, float h)
        {
            return h * r1 * r1 * pi;
        }


    }
}

