﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathe_Bibl_
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //A(1): Berechnung von einer Quadratischen Gleichung
            Console.WriteLine("Berechnung von Quadratischen Gleichung: aX²+bX+c");

            double a, b, c;
            Console.Write("a="); a = float.Parse(Console.ReadLine());
            Console.Write("b="); b = float.Parse(Console.ReadLine());
            Console.Write("c="); c = float.Parse(Console.ReadLine());
            Quad_Gleichung(a, b, c);

            //A(2): Zahl Vorzeichen überprüfen
            Console.WriteLine("\nÜberprüfe, ob eine Zahl negativ, positiv oder Null ist");
            float x;
            Console.Write("x="); x = float.Parse(Console.ReadLine());
            Console.WriteLine("Die Zahl ist {0}", Zahl_check(x));

            //A(3): Max von drei Zahlen ausgeben
            Console.WriteLine("\nMax von drei Zahlen ausgeben");
            float a1, b1, c1;
            Console.Write("a="); a1 = float.Parse(Console.ReadLine());
            Console.Write("b="); b1 = float.Parse(Console.ReadLine());
            Console.Write("c="); c1 = float.Parse(Console.ReadLine());
            Console.WriteLine("max Zahl= {0}", Max(a1, b1, c1));

            //A(4): Note überprüfen mit Bewertungsausgabe:
            Console.WriteLine("\nNote überprüfen mit Bewertungsausgabe");
            int Note;
            Console.Write("Note="); Note = int.Parse(Console.ReadLine());
            Console.WriteLine("Bewertung: {0}", Note_check(Note));

            //A(5): Verkaufsprovision rechnen
            Console.WriteLine("\nVerkaufsprovision(v) = Gesamtumsatz(s) * Provision in Prozent");
            double s;
            Console.Write("Gesamtumsatz="); s = double.Parse(Console.ReadLine());
            Console.WriteLine("Verkaufsprovision: {0}", Verkaufsprovision(s));
        }

        static void Quad_Gleichung(double a, double b, double c)
        {
            double D = Math.Pow(b, 2) - 4 * a * c;

            if (D > 0)
            {
                Console.WriteLine("X1= {0}", (-b + Math.Sqrt(D)) / (2 * a));
                Console.WriteLine("X2= {0}", (-b - Math.Sqrt(D)) / (2 * a));
            }

            else if (D == 0)
            {
                Console.WriteLine("X1 = X2 = {0}", -b / (2 * a));
            }
            //Console.WriteLine("{0} {1} {2} \n{3}, a, b, c, D);
            else
                Console.WriteLine("{0} {1} {2} \n{3}\nKeine Lösung D < 0", a, b, c, D);
        }
        static string Zahl_check(float x)
        {
            if (x > 0) return "positiv";
            else if (x == 0) return "Null";
            else return "negativ";
        }
        static float Max(float a, float b, float c)
        {
            float max = a;
            if (b > max) { max = b; }
            if (c> max) { max = c; }
            return max;
        }
        static string Note_check(int Note)
        {
            if (Note <= 100 & Note >= 0)
            {
                if (Note >= 90)
                    return "A";
                else if (Note >= 80)
                    return "B";
                else if (Note >= 70)
                    return "C";
                else if (Note >= 60)
                    return "D";
                else if (Note >= 50)
                    return "E";
                else
                    return "durchgefallen";
            }

            else return "error: Gibt eine gültige Note!";
        }
        static double Verkaufsprovision(double s)
        {
            double v;
            //Gesamtumsatz<5000 => Provision in Prozent=2%
            if (s < 5000) v = s * 2 / 100;
            else if (s == 5000) v = s * 5 / 100;
            else v = s * 8 / 100;
            return v;
        }
    }
}