﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace switchs;
internal class Program
{
    static void Main(string[] args)
    {
        // Nummer der Tagen im März, die dem eingegebenen Tag entsprechen
        string day;
        Console.Write("Tag? ");
        day= Console.ReadLine();

        switch(day)
        {
            case "Montag": Console.WriteLine("6, 13, 20, 27"); break;
            case "Dienstag": Console.WriteLine("7, 14, 21, 28"); break;
            case "Mittwoch": Console.WriteLine("1, 8, 15, 22, 29"); break;
            case "Donnerstag": Console.WriteLine("2, 9, 16, 23, 30"); break;
            case "Freitag": Console.WriteLine("3, 10, 17, 24, 31"); break;
            case "Samstag": Console.WriteLine("4, 11, 18, 25"); break;
            case "Sonntag": Console.WriteLine("5, 12, 19, 26"); break;
            default: Console.WriteLine("Geben Sie bitte einen gültigen Tag!"); break;
        }

        //A(2): Überprüfung der Note und Bewertungsausgeben
        Console.WriteLine("\nNote überprüfen mit Bewertungsausgabe");
        int Note;
        Console.Write("Note="); Note = int.Parse(Console.ReadLine());
        Console.WriteLine("Bewertung: {0}", Note_check(Note));

        //A(3): Jahreszeiten ausgeben: 1:Sommer, 2: Frühling, 3: Winter, 4: Herbst
        int x;
        Console.Write("\nJahreszeit-Nummer: "); x = int.Parse(Console.ReadLine());
        Console.WriteLine("Jahreszeit: {0}", season(x));

        //A(3): 1: eins ausgeben, 2:zwei ausgeben, 3: drei ausgeben, sonst nein
        int y;
        Console.WriteLine("\n1, 2, oder 3 als Name ausgeben, sonst nein ausgeben");
        Console.Write("\nNummer: "); y = int.Parse(Console.ReadLine());
        Console.WriteLine("{0}", Nummer(y));

        //A(4):Zahl überpüfen, ob sie eine gerade oder ungerade Zahl
        int z;
        Console.WriteLine("\nZahl überpüfen, ob sie eine gerade oder ungerade Zahl");
        Console.Write("\nNummer: "); z = int.Parse(Console.ReadLine());
        Console.WriteLine("{0}", gerade(z));
    }
    static string Note_check(int Note)
    {
        if (Note > 100 | Note < 0) return "error: Gibe bitte eine gültige Note!";
        
        else switch (Note)
            {
                case >= 90: return "A";
                case >= 80: return "B";
                case >= 70: return "C";
                case >= 60: return "D";
                case >= 50: return "E";
                default: return "durchgefallen";
            }
/*          oder:
            return Note switch
            {
                >= 90 => "A",
                >= 80 => "B",
                >= 70 => "C",
                >= 60 => "D",
                >= 50 => "E",
                _ => "durchgefallen",
            };*/
    }
    static string season(int x)
    {
        return x switch
        {
            1 => "Sommer",
            2 => "Frühling",
            3 => "Winter",
            4 => "Herbst",
            _ => "Gib bitte eine gültige Jahreszeit!"
        };
    }
    static string Nummer(int y)
    {
        return y switch
        {
            1 => "eins",
            2 => "zwei",
            3 => "drei",
            _ => "Nein"
        };
    }

    static string gerade(int y)
    {
        return (y % 2) switch
        {
            0 => "gerade",
            _ => "ungerade"
        };
    }
}
